<?php

use App\Http\Controllers\Admin\ConfigurationController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Front\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware(['auth']);

Route::middleware(['auth', 'available'])->name('front.')->group(function () {
    Route::get('quiz', [DashboardController::class, 'getQuiz'])->name('quiz');
});

Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('/questions/data', [QuestionController::class, 'data'])->name('questions.data');
    Route::resource('questions', QuestionController::class);

    Route::get('/configurations', [ConfigurationController::class, 'index'])->name('configurations.index');
    Route::put('/configurations/{id}', [ConfigurationController::class, 'update'])->name('configurations.update');
});

require __DIR__.'/auth.php';
