<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Questions\StoreRequest;
use App\Http\Requests\Questions\UpdateRequest;
use App\Models\Question;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, $next) {
            if (auth()->user()->is_admin !== 1){
                abort('404');
            }
            return $next($request);
        });
    }

    public function index()
    {
        return view('admin.questions.index');
    }

    public function create()
    {
        $view = view('admin.partials.add-question')->render();

        return response(['html' => $view]);
    }

    public function store(StoreRequest $request)
    {
        $question = new Question();

        $optionArr = [
            'a' => $request->option1,
            'b' => $request->option2,
            'c' => $request->option3,
            'd' => $request->option4,
        ];

        $question->question = $request->question;
        $question->options = $optionArr;
        $question->correct_answer = $request->correct_ans;

        $question->save();

        return redirect(route('admin.questions.index'));
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);

        $view = view('admin.partials.edit-question', compact('question'))->render();

        return response(['html' => $view]);
    }

    public function update(UpdateRequest $request, $id)
    {
        $question = Question::findOrFail($id);

        $optionArr = [
            'a' => $request->option1,
            'b' => $request->option2,
            'c' => $request->option3,
            'd' => $request->option4,
        ];

        $question->question = $request->question;
        $question->options = $optionArr;
        $question->correct_answer = $request->correct_ans;

        $question->save();

        return redirect(route('admin.questions.index'));
    }

    public function destroy($id)
    {
        Question::destroy($id);

        return response([]);
    }

    public function data()
    {
        $questions = Question::all();
        // $questions = User::all();

        return DataTables::of($questions)
                        ->addIndexColumn()
                        ->addColumn('actions', function ($row)
                        {
                            $actions = "<a href='javascript:editQuestion(".$row->id.");'>Edit</a> <a href='javascript:deleteQuestion(".$row->id.");'>Delete</a>";
                            return $actions;
                        })
                        ->addColumn('question', function ($row) {
                            return $row->question;
                        })
                        ->addColumn('options', function ($row) {
                            $options = '<ul>';
                            foreach ($row->options as $key => $value) {
                                $options .= '<li>'.$key.':'.$value.'</li>';
                            }
                            $options .= '</ul>';
                            return $options;
                        })
                        ->addColumn('correct_answer', function ($row) {
                            return $row->correct_answer;
                        })
                        ->rawColumns(['actions', 'options'])
                        ->make(true);
    }
}
