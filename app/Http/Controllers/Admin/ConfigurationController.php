<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Configuration;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    public function __construct()
    {
        $this->middleware(function($request, $next) {
            if (auth()->user()->is_admin !== 1){
                abort('404');
            }
            return $next($request);
        });
    }

    public function index()
    {
        $configuration = Configuration::first();

        return view('admin.configurations.index', compact('configuration'));
    }

    public function update(Request $request, $id)
    {
        $configuration = Configuration::find($id);

        $configuration->questions_per_day = $request->questions_per_day;
        $configuration->coins_per_correct_ans = $request->coins_per_correct_ans;
        $configuration->hours_for_next_questions = $request->hours_for_next_questions;

        $configuration->save();

        return redirect(route('admin.configurations.index'));
    }
}
