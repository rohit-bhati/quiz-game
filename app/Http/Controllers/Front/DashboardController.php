<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Configuration;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $config = Configuration::first();
        $hoursForNextQuestion = $config->hours_for_next_questions;
        $quizes = auth()->user()->quizes;
        $dateAttempted = $quizes->values()->last()->date_attempted;
        $disabled = true;
        if($quizes->count() == 0) {
            $disabled = false;
            return view('front.index', compact('disabled'));
        }
        else {
            if(Carbon::now()->diffInHours($dateAttempted) >= $hoursForNextQuestion) {
                $disabled = false;
                return view('front.index', compact('disabled'));
            }
            return view('front.index', compact('disabled'));
        }
    }

    public function getQuiz()
    {
        // auth()->user()->quizes->values()->last()->
    }
}
