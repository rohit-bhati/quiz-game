<?php

namespace App\Http\Middleware;

use App\Models\Configuration;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class AvailableForQuiz
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $config = Configuration::first();
        $hoursForNextQuestion = $config->hours_for_next_questions;
        $quizes = auth()->user()->quizes;
        if($quizes->count() == 0) {
            return $next($request);
        }
        else {
            if(Carbon::now()->diffInHours($quizes->values()->last()->date_attempted) >= $hoursForNextQuestion) {
                return $next($request);
            }
            abort('404');
        }

    }
}
