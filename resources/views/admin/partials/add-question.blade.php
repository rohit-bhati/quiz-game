<form class="m-3 p-3 border" action="{{ route('admin.questions.store') }}" method="post">
    @csrf
    <h2>Add New Question</h2>
    <div class="m-3">
        <label for="question">Question</label>
        <input type="text" name="question" id="question">
    </div>
    <div class="m-3">
        <label for="options">Options</label>
        <input type="text" name="option1" id="option1">
        <input type="text" name="option2" id="option2">
        <input type="text" name="option3" id="option3">
        <input type="text" name="option4" id="option4">
    </div>
    <div class="m-3">
        <label for="correct-ans">Correct Option</label>
        <input type="text" name="correct_ans" id="correct_ans">
    </div>
    <button class="p-2 rounded bg-blue-500 text-white">Save</button>
</form>