<form class="m-3 p-3 border" action="{{ route('admin.questions.update', $question->id) }}" method="post">
    @csrf
    @method('PUT')
    <h2>Edit Question</h2>
    <div class="m-3">
        <label for="question">Question</label>
        <input type="text" name="question" id="question" value="{{ $question->question }}">
    </div>
    <div class="m-3">
        <label for="options">Options</label>
        <input type="text" name="option1" id="option1" value="{{ $question->options['a'] }}">
        <input type="text" name="option2" id="option2" value="{{ $question->options['b'] }}">
        <input type="text" name="option3" id="option3" value="{{ $question->options['c'] }}">
        <input type="text" name="option4" id="option4" value="{{ $question->options['d'] }}">
    </div>
    <div class="m-3">
        <label for="correct-ans">Correct Option</label>
        <input type="text" name="correct_ans" id="correct_ans" value="{{ $question->correct_answer }}">
    </div>
    <button class="p-2 rounded bg-blue-500 text-white">Update</button>
</form>