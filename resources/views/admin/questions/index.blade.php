<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <a class="mr-20" href="{{route('admin.questions.index')}}">Quiz Questions</a>
            <a href="{{route('admin.configurations.index')}}">Configurations</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <button class="p-2 m-2 border border-gray-200" onclick="javascript:addQuestion();">Add New</button>
                    <div id="add-edit-form">
                        @include('admin.partials.add-question')
                    </div>
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Question</th>
                                <th>Options</th>
                                <th>Correct Answer</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            var usersTable;
            // $(function() {
                usersTable = $('#users-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{!! route('admin.questions.data') !!}",
                    columns: [
                        { data: 'id', name: 'id' },
                        { data: 'question', name: 'question' },
                        { data: 'options', name: 'options' },
                        { data: 'correct_answer', name: 'correct_answer' },
                        { data: 'actions', name: 'actions' }
                    ]
                });
            // });

            function addQuestion() {
                var url = "{{ route('admin.questions.create') }}"

                $.ajax({
                    url,
                    success: function (data) {
                        $('#add-edit-form').html(data.html)
                    }
                })
            }

            function editQuestion(id) {
                var url = "{{ route('admin.questions.edit', ':id') }}"
                url = url.replace(':id', id)

                $.ajax({
                    url,
                    success: function (data) {
                        $('#add-edit-form').html(data.html)
                    }
                })
            }

            function deleteQuestion(id) {
                var url = "{{ route('admin.questions.destroy', ':id') }}"
                url = url.replace(':id', id)

                $.ajax({
                    url,
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        _method: 'DELETE'
                    },
                    success: function (data) {
                        usersTable.draw()
                    }
                })
            }
        </script>
    @endpush
</x-app-layout>