<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            <a class="mr-20" href="{{route('admin.questions.index')}}">Quiz Questions</a>
            <a href="{{route('admin.configurations.index')}}">Configurations</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('admin.configurations.update', $configuration->id) }}" method="post">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="qpd">Questions Per Day</label>
                            <input class="rounded mb-5" type="text" name="questions_per_day" id="qpd" value="{{ $configuration->questions_per_day }}" />
                        </div>

                        <div class="form-group">
                            <label for="cpca">Coins Per Correct Answer</label>
                            <input class="rounded mb-5" type="text" name="coins_per_correct_ans" id="cpca" value="{{ $configuration->coins_per_correct_ans }}" />
                        </div>

                        <div class="form-group">
                            <label for="hfnq">Hours for Next Question Set</label>
                            <input class="rounded mb-5" type="text" name="hours_for_next_questions" id="hfnq" value="{{ $configuration->hours_for_next_questions }}" />
                        </div>

                        <button class="bg-blue-500 text-white p-3" type="submit">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>