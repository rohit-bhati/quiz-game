<x-guest-layout>
    <button
    <?php if ($disabled) { ?> disabled <?php } ?>
    class="border bg-blue-500 p-2 text-white {{ $disabled ? 'cursor-not-allowed' : '' }}" onclick="javascript:startQuiz();">Start Quiz</button>

    @push('scripts')
        <script>
            function startQuiz() {
                window.location.href = "{{ route('front.quiz') }}"
            }
        </script>
    @endpush
</x-guest-layout>